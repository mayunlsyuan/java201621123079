package week10;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class  pta61 {

     public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextLine()){
            List<String> list = convertStringToList(sc.nextLine());
            System.out.println(list);
            String word = sc.nextLine();
            remove(list,word);
            System.out.println(list);
        }
        sc.close();
    }

     private static void remove(List<String> list, String word) {
         String[] arr = word.split(" +");
         for (int i = 0; i < arr.length; i++) {
             for (int j = 0; j < list.size(); j++) {
                 if(arr[i].equals(list.get(j))) {
                     list.remove(j--);
                    
                 }
             }
         }
     }

     private static List<String> convertStringToList(String nextLine) {

         List<String> li = new ArrayList<String>();
         String[] arr = nextLine.split(" +");
         for (int i = 0; i < arr.length; i++) {
             li.add(arr[i]);
         }
         return li;
     }
    
}