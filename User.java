package Library;

import java.util.ArrayList;

public class User {
	public String name;
	public long no;
	private String password;
	public boolean isAdmin;
	
	private static ArrayList<User> userList;

	public User(String name,long no,String pass,boolean isAdmin) {
		this.name = name;
		this.password = pass;
		this.no = no;
		this.isAdmin = isAdmin;
	}
	
	public User() {
		userList = new ArrayList<User>();
	}
	
	public static void reg(String name,long no,String isA,boolean isAdmin) {
		userList.add(new User(name,no,isA,isAdmin));
	}

	public static User login(long no,String password) {
		for (int i = 0; i < userList.size(); i++) {
			if(no == userList.get(i).no) 
				if(userList.get(i).password.equals(password)) return userList.get(i);
		}
		return null;
	}
	
	public boolean borrow(long no) {
		if(BookList.searchNo(no)==-1) return false;
		BookList.borrow(no, this.no);
		return true;
	}
	
	public void ret(long no) {
		BookList.returnBook(no, this.no);
	}
	
	public void add(String name,long no,String author,boolean isBorrow,long boNo) {
		BookList.add(name, no, author, isBorrow, boNo);
	}
	
	
}
