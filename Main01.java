package week07;
import java.util.Scanner;

public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);{
    while(sc.hasNextLine()){
        String line = sc.nextLine();
        String result = null;
        if(line.equals("null"))
            result = extractBirthDate(null);
        else
            result = extractBirthDate(line);
        if(result==null)
            continue;
        System.out.println(result);
    }
    
}

private static String extractBirthDate(String nextLine) {
    if(nextLine==null) 
    return null;
    String str=nextLine.replace(" ","");
    if(str==null || str.length()<18) 
    return null;
    String year = str.substring(6, 10);
    String month = str.substring(10, 12);
    String day = str.substring(12, 14);
    return year + "-" + month + "-" + day;
}





