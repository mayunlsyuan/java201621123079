package week07;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

class NameComparator implements Comparator<PersonSortable>{

    @Override
    public int compare(PersonSortable o1, PersonSortable o2) {
        return o1.getName().compareTo(o2.getName());
    }
    
}

class AgeComparator implements Comparator<PersonSortable>{

    @Override
    public int compare(PersonSortable o1, PersonSortable o2) {
        return o1.getAge()-o2.getAge();
    }
    
}

class PersonSortable implements Comparable<PersonSortable>{
    private String name;
    private int age;
    public int compareTo(PersonSortable o){
        if(!name.equals(o.name)) return name.compareTo(o.name);
        return age-o.age;
    }
    public PersonSortable(String name , int age) {
        this.name = name;
        this.age = age;
    }
    @Override
    public String toString() {
        return name + "-" + age ;
    }
    public String getName(){
        return name;
    }
    public int getAge(){
        return age;
    }
    
}

public class pta {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        PersonSortable[] stus = new PersonSortable[n];
        for (int i = 0; i < stus.length; i++) {
            stus[i] = new PersonSortable(sc.next(),sc.nextInt());
            
        }
       
        
        
        
        Arrays.sort(stus,(PersonSortable p1,PersonSortable p2)->p1.getName().compareTo(p2.getName()));
        System.out.println("201621123079 ��˼Զ");
        System.out.println("NameComparator:sort");
        for (int i = 0; i < stus.length; i++) {
            System.out.println(stus[i].toString());
        }
        Arrays.sort(stus,(PersonSortable p1,PersonSortable p2)->(p1.getAge()-p2.getAge()));
        System.out.println("AgeComparator:sort");
        for (int i = 0; i < stus.length; i++) {
            System.out.println(stus[i].toString());
        }
        System.out.println(Arrays.toString(NameComparator.class.getInterfaces()));
        System.out.println(Arrays.toString(AgeComparator.class.getInterfaces()));
    }
}