package week10;

import java.util.*;
import java.util.Map.Entry;


public class Pta73
{

	public static void main(String[] args) {
		HashMap<String ,Integer> dict = new HashMap<String ,Integer>();
		Scanner sc = new Scanner(System.in );
		while(sc.hasNextLine()){
			String  line = sc.nextLine();
			if("!!!!!".equals(line)) break;
			String[] words = line.split(" +");
			for (int i = 0; i < words.length; i++) {
				if(dict.containsKey(words[i])) {
					dict.put(words[i], dict.get(words[i])+1);
				}
				else {
					dict.put(words[i], 1);
				}
			}
		}
		System.out.println(dict.size());
		List<Entry<String,Integer>> list =new ArrayList<Entry<String,Integer>>(dict.entrySet());
		Collections.sort(list,new Comparator<Entry<String,Integer>>(){

			@Override
			public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
				if(((Integer) o1.getValue()).compareTo((Integer) o2.getValue()) == 0) return ((String) o1.getKey()).compareTo((String ) o2.getKey());
				return ((Integer) o2.getValue()).compareTo((Integer) o1.getValue());
			}
		
		});

		List<Entry<String, Integer>> list1 = list.subList(0, 10); 
        for (Map.Entry<String, Integer> item : list1)  
            System.out.println(item.getKey() + "=" + item.getValue());  
        sc.close();
	}

}
