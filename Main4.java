package week02;

import java.util.Arrays;
import java.util.Scanner;

public class Main4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		String[][] str = getDynamicMatrix(n);
		System.out.println(Arrays.deepToString(str));
	}

	private static String[][] getDynamicMatrix(int n) {
		String[][] strs = new String[n][];
		
		for (int i = 0; i < n; i++) {
			strs[i] = new String[i+1];
			
		}
		
		return strs;

	}

}
