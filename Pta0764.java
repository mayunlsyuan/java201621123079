package week12;

class Account{
	private int balance;
	
	public Account(int money) {
		balance = money;
	}
	
	public synchronized void deposit(int money) {
		balance += money;
	}
	public synchronized void withdraw(int money) {
		balance -= money;
	}
	
	public int getBalance() {
		return balance;
	}
}
