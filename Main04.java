
package week10 ;

import java.util.*;
import java.util.stream.Collectors;

enum Gender{
	man,woman;
}

class Student{
	private Long id;
	private String name;
	private int age;
	private Gender gender;
	private boolean joinsACM;

	
	public Student(Long id, String name, int age, Gender gender, boolean joinsACM) {
		this.id = id;
		this.name =name;
		this.age=age;
		this.gender=gender;
		this.joinsACM=joinsACM;
	}

	public static List<Student> search(List<Student> stuList, Long id, String name, int age, Gender gender, boolean joinsACM){
		List<Student> students = new ArrayList<Student>();
		for (int i = 0; i < stuList.size(); i++) {
			if(stuList.get(i).id>id&&stuList.get(i).name.equals(name)&&stuList.get(i).age>age&&stuList.get(i).gender==gender&&stuList.get(i).joinsACM==joinsACM) 
				students.add(stuList.get(i));
		}
		return students;
	}

	
	
	@Override
	public String toString() {
		return "Student [id>" + id + ", name>" + name + ", age>" + age + ", gender>" + gender + ", joinsACM>" + joinsACM + "]";
	}
	
	public static List<Student> searchStream(List<Student> stuList, Long id, String name, int age, Gender gender, boolean joinsACM){
		List<Student> result = stuList.stream().filter
				(student->student!=null&&student.id 

>id&&student.name 

.equals(name)&&student.age>age&&student.gender==gender&&student.joinsACM==joinsACM)
				.collect(Collectors.toList());
		return result;

	}
	
}   

public class Main04 {

	public static void main(String[] args) {
		List<Student> student = new ArrayList<Student>();
		student.add(new Student(10l,"wzg",10,Gender.man,true));
		student.add(new Student(11l,"cjb",11,Gender.woman,true));
		student.add(new Student(11l,"lsy",11,Gender.man,true));
		student.add(new Student(13l,"zsy",12,Gender.woman,true));
		student.add(new Student(14l,"zhy",10,Gender.man,false));
		System.out.println(Student.search(student, 10l,"lsy",10,Gender.man,true));
		System.out.println("201621123079��˼Զ");
	}

}
