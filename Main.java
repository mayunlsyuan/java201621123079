package shop;

import java.util.*;

class Goods{
	private String name;
	private double price;
	
	public Goods(String name,double price) {
		this.name = name;
		this.price = price;
	}
	
	public String getName(){
		return name;
	}
	
	public double getPrice(){
		return price;
	}

	@Override
	public String toString() {
		return "商品 [名称=" + name + " 价格=" + price + "]";
	}
	
	
}

class Mart{
	
	static private ArrayList<Goods> List = new ArrayList<Goods>();
	
	public void addGoods(String name,double price) {
		Goods goods = new Goods(name,price);
		List.add(goods);
	}
	
	public void Show(){
		System.out.println("商品列表:");
		for (int i = 0; i < List.size(); i++) {
			System.out.println("商品序号： "+ i + " " + List.get(i).toString());
		}
	}
	
	public void search(String name) {
		boolean f = false;
		System.out.println("搜索结果:");
		for (int i = 0; i < List.size(); i++) {
			if(name.equals(List.get(i).getName())) {
				System.out.println("商品序号： "+ i + " " + List.get(i).toString());
				f = true ;
				break;
			}
		}
		if(!f) System.out.println("Not Found!");
	}
	
	public Goods getOne(int i) {
		//System.out.println(List.get(i).getName());
		return List.get(i);
	}
}

class User{
	private String name;
	private String password;
	Car car;
	
	public User(String name,String password) {
		this.name=name;
		this.password=password;
		car = new Car();
	}
	
	public String getUserName() {
		return name;
	}
	
	public String getUserPassword() {
		return password;
	}
}

class UserList{
	static private ArrayList<User> user = new ArrayList<User>();
	
	public void addUser(String name,String password) {
		user.add(new User(name,password));		
	}
	
	public boolean Login(String name,String password){
		for (int i = 0; i < user.size(); i++) {
			if(name.equals(user.get(i).getUserName())) {
				if(password.equals(user.get(i).getUserPassword())) return true;
			}
		}
		System.out.println("登录失败");
		return false;
	}
	
	public int GetIn(String name) {
		for (int i = 0; i < user.size(); i++) {
			if(name.equals(user.get(i).getUserName())) {
				return i;
			}
		}
		return -1;
	}
	
	public User UserCar(int i) {
		return user.get(i);
	}
}

class Car extends Mart{
	private ArrayList<Goods> gList = new ArrayList<Goods>();
	private ArrayList numList = new ArrayList();
	private double allPrice = 0;
	
	public void addGoodsInCar(int i,int num) {
		gList.add(getOne(i));
		numList.add(num);
		allPrice += (num*gList.get(gList.size()-1).getPrice());
	}
	
	public void removeGoods(int i) {
		allPrice -= ((gList.get(i).getPrice())*((double)numList.get(i)));
		gList.remove(i);
		numList.remove(i);
	}
	
	public void ShowCar() {
		System.out.println("购物车:");
		for (int i = 0; i < gList.size(); i++) {
			System.out.println(gList.get(i).toString() + " 数量: " + numList.get(i));
		}
		System.out.println("总价: " + allPrice);
	}
	
	public void Pay() {
		gList.clear();
		numList.clear();
		allPrice = 0 ;
	}
}

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Mart mart = new Mart();
		UserList us = new UserList();
		boolean f = true;
		int in = -1;
		mart.addGoods("iphone 8", 6788.8);
		mart.addGoods("iphone 8 plus", 8788.8);
		mart.addGoods("iphone 7", 5288.8);
		mart.addGoods("iphone 7 plus", 5888.8);
		mart.addGoods("mate 10", 4888.8);
		mart.addGoods("ear", 150);
		//mart.Show();
		//System.out.println("1. 注册 2. 登录");
		while(f) {
			System.out.println("1. 注册 2. 登录");
			int n = sc.nextInt();
			String s1 = sc.next();
			String s2 = sc.next();
			switch (n) {
			case 1:
				us.addUser(s1, s2);
			default:
				if(us.Login(s1, s2)) {
					f=false;
					in = us.GetIn(s1);
				}
				break;
			}
		}
		//System.out.printf("1.查看所有商品 \n2.搜索\n3.添加商品到购物车 \n4.查看购物车\n5.付款\n其它。推出\n");
		/*System.out.println("1.查看所有商品 ");
		System.out.println("2.搜索");
		System.out.println("3.添加商品到购物车");
		System.out.println("4.查看购物车");
		System.out.println("5.付款");*/
		f=true;
		while(f) {
			System.out.printf("1.查看所有商品 \n2.搜索\n3.添加商品到购物车 \n4.查看购物车\n5.付款\n其它。推出\n");
			int n = sc.nextInt();
			switch (n) {
			case 1:
				mart.Show();
				break;
			case 2:
				System.out.println("商品名：");
				sc.nextLine();
				String st = sc.nextLine();
				System.out.println(st);
				mart.search(st);
				break;	
			case 3:
				System.out.println("商品序号：");
				int i = sc.nextInt();
				us.UserCar(in).car.addGoodsInCar(i, sc.nextInt());
				break;
			case 4:
				us.UserCar(in).car.ShowCar();
				break;
			case 5:
				us.UserCar(in).car.Pay();
				break;
			default:
				f = false;
				break;
			}
		}
		
		sc.close();
	}

}
