package week09;

import java.util.*;

interface IntegerStack {
    public Integer push(Integer item);
    public Integer pop();                   
    public Integer peek();           
    public boolean empty();               
    public int size();                       
}
class ArrayListIntegerStack implements IntegerStack{

    private List<Integer> list = new ArrayList<Integer>();
   
    @Override
    public Integer push(Integer item) {
        // TODO Auto-generated method stub
        if(item == null) 
        	return null;
        list.add(item);
        return item;
    }

    @Override
    public Integer pop() {
        // TODO Auto-generated method stub
        if(list.size() == 0) return null;
        Integer in = list.get(list.size()-1);
        list.remove(list.size()-1);
        return in ;
    }

    @Override
    public Integer peek() {
        // TODO Auto-generated method stub
        if(list.size() == 0) return null;
        return list.get(list.size()-1);
    }

    @Override
    public boolean empty() {
        // TODO Auto-generated method stub
        if(list.size() == 0) 
        	return true;
        return false;
    }

    @Override
    public int size() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public String toString() {
        return list.toString();
    }

   
}

public class Pta71 {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        ArrayListIntegerStack arr = new ArrayListIntegerStack();
        Scanner sc = new Scanner (System.in);
        int m = sc.nextInt();
        for (int i = 0; i < m; i++) {
            System.out.println(arr.push(sc.nextInt()));
        }   
        System.out.println("" + arr.peek() + "," + arr.empty() + "," + arr.size());
        System.out.println(arr.toString());
        m = sc.nextInt();
        for (int i = 0; i < m; i++) {
            System.out.println(arr.pop());
        }
        System.out.println("" + arr.peek() + "," + arr.empty() + "," + arr.size());
        System.out.println(arr.toString());
        sc.close();
    }

}
