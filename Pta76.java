package week09;
import java.util.*;

public class  Pta76{

	public static void main(String[] args) {
		LinkedList<Integer> Q1 = new LinkedList<Integer>();
		LinkedList<Integer> Q2 = new LinkedList<Integer>();
		Scanner sc = new Scanner (System.in);
		int a = sc.nextInt();
		int temp ;
		boolean f = true;
		if(a > 1000 || a <= 0) System.exit(0);
		for (int i = 0; i < a; i++) {
			temp = sc.nextInt();
			if(temp%2 == 0) { 
				Q2.add(temp);
			}
			else Q1.add(temp);
			if(Q1.size() == 2 ||Q2.size() == 2) {
				while(Q1.size() != 0) {
					if(f) {
						System.out.print(Q1.poll());
						f = false;
					}
					else System.out.print(" "+Q1.poll());
				}
				while(Q2.size() != 0) {
					if(f) {
						System.out.print(Q2.poll());
						f = false;
					}
					System.out.print(" "+Q2.poll());
				}
			}
		}
		sc.close();
		
	}

}

