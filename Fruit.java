package week04;

import java.util.ArrayList;

class Fruit {
    private String name;
    public Fruit(String name){
        this.name = name;
    }
    public String toString() {
        return name+"-"+super.toString();
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object ob) {
        if (this == ob)
            return true;
        if (ob == null)
            return false;
        if (getClass() != ob.getClass())
            return false;
        Fruit other = (Fruit) ob;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equalsIgnoreCase(other.name))
            return false;
        return true;
    }
    

	public static void main(String[] args) {
		Fruit[] fruit = new Fruit[5];
		fruit[0]=new Fruit("orange");
		fruit[1]=new Fruit("apple");
		fruit[2]=new Fruit("banana");
		fruit[3]=new Fruit("bear");
		fruit[4]=new Fruit("watermalon");
		ArrayList<Fruit> fruits= new ArrayList<>();
		for(int i=0;i<fruit.length;i++)
		{
			if(fruits.contains(fruit[i])==false)
			fruits.add(fruit[i]);
			
		}
		System.out.println(fruits);
	}

}
