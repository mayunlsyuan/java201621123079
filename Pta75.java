package week10;

import java.awt.List;
import java.util.ArrayList;
import java.util.Scanner;

interface GeneralStack<E>{
	E push(E item);            //如item为null，则不入栈直接返回null。
	E pop();                 //出栈，如为栈为空，则返回null。
	E peek();                //获得栈顶元素，如为空，则返回null.
	public boolean empty();//如为空返回true
	public int size();     //返回栈中元素数量
}

class ArrayListGeneralStack<E> implements GeneralStack<E>{

	ArrayList<E> list = new ArrayList<E>();
	
	@Override
	public E push(E item) {
		list.add(item);
		System.out.println("push:"+item);
		return item;
	}

	@Override
	public E pop() {
		// TODO Auto-generated method stub
		if(list.size()==0) return null;
		E item = list.get(list.size()-1);
		list.remove(list.size()-1);
		System.out.println("pop:"+item);
		return item;
	}

	@Override
	public E peek() {
		// TODO Auto-generated method stub
		if(list.size()==0) return null;
		System.out.println("peek:"+list.get(list.size()-1));
		return list.get(list.size()-1);
	}

	@Override
	public boolean empty() {
		// TODO Auto-generated method stub
		if(list.size()==0) return true;
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public String toString() {
		return list.toString();
	}
	
	public E get(int i) {
		return list.get(i);
	}
}

class Car{
	private int id;
	private String name;
	public Car(int id, String name) {
		this.id =id;
		this.name =name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name 

 = name;
	}
	@Override
	public String toString() {
		return "Car [id=" + id + ", name=" + name + "]";
	}
	
	
}

public class Pta75 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in );
		boolean f = true;
		while(f) {
			String str = sc.next();
			int m = sc.nextInt();
			int n = sc.nextInt();
			switch (str) {
			case "Car":
				ArrayListGeneralStack<Car> list1 = new ArrayListGeneralStack<Car>();
				System.out.println("Car Test");
				for (int i = 0; i < m; i++) {
					list1.push(new Car(sc.nextInt(),sc.next()));
				}
				for (int i = 0; i < n; i++) {
					list1.pop();
				}
				System.out.println(list1.toString());
				String sum1 = "";
				for (int i = 0; i < list1.size(); i++) {
					sum1 += list1.get(i).getName();
				}
				System.out.println("sum="+sum1);
				System.out.println(list1.getClass().getInterfaces()[0]);
				break;
			case "Integer":
				ArrayListGeneralStack<Integer> list2 = new ArrayListGeneralStack<Integer>();
				System.out.println("Integer Test");
				for (int i = 0; i < m; i++) {
					list2.push(sc.nextInt());
				}
				for (int i = 0; i < n; i++) {
					list2.pop();
				}
				System.out.println(list2.toString());
				int sum2 = 0;
				for (int i = 0; i < list2.size(); i++) {
					sum2 += list2.get(i);
				}
				System.out.println("sum="+sum2);
				System.out.println(list2.getClass().getInterfaces()[0]);
				break;
			case "Double":
				ArrayListGeneralStack<Double> list3 = new ArrayListGeneralStack<Double>();
				System.out.println("Double Test");
				for (int i = 0; i < m; i++) {
					list3.push(sc.nextDouble());
				}
				for (int i = 0; i < n; i++) {
					list3.pop();
				}
				System.out.println(list3.toString());
				double sum3 = 0.0;
				for (int i = 0; i < list3.size(); i++) {
					sum3 += list3.get(i);
				}
				System.out.println("sum="+sum3);
				System.out.println(list3.getClass().getInterfaces()[0]);
				break;
			default:
				f = false;
				break;
			}
		}
		sc.close();
	}

}
