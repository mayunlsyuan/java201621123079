package week11;

import java.io .FileInputStream;
import java.io .FileNotFoundException;
import java.io .IOException;
import java.util.Arrays;
import java.util.Scanner;

public class W11Main4 {
	public static void main(String[] args) throws IOException {
		byte[] content = null;
		FileInputStream fis = null;
		boolean f = true;
		boolean ff = true;
		Scanner sc = new Scanner (System.in 

);
		while (f) {
			try {
				// FileInputStream fis =null;
				if(ff) {
					fis = new FileInputStream("testfis.txt");
				}
				else {
					System.out.println("重新輸入");
					String str = sc.nextLine();
					f = false;
					fis = new FileInputStream(str);
				}
				int bytesAvailabe = fis.available();// 获得该文件可用的字节数
				if (bytesAvailabe > 0) {
					content = new byte[bytesAvailabe];// 创建可容纳文件大小的数组
					fis.read(content);// 将文件内容读入数组
				}
				System.out.println(Arrays.toString(content));// 打印数组内容
				f = false;
			} catch (FileNotFoundException e) {
				System.out.println(e);
				f = true;
			} catch (IOException e) {
				System.out.println(e);
			} finally {
				try {
					fis.close();
				} catch (Exception e) {
					System.out.println(e);
				}
				ff = false;
			}
		}
	}
}
