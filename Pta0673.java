package week11;

import java.util.Scanner;

class ArrayUtils{
	
	public static double findMax(double[] arr,int begin, int end) throws IllegalArgumentException{
		
		if(begin >= end) 
			throw new IllegalArgumentException("begin:"+begin+" >= end:"+end);
		if(begin < 0) 
			throw new IllegalArgumentException("begin:"+begin+" < 0");
		if(end > arr.length)
			throw new IllegalArgumentException("end:"+end+" > arr.length");
		double max = arr[begin];
		for (int i = begin; i < end; i++) {
			if(max < arr[i]) max = arr[i];
		}
		return max;
		
	}
}

public class Pta0673 {

	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in );
			
			int n = sc.nextInt();
			double [] ar = new double [n];
			for (int i = 0; i < ar.length; i++) {
				ar[i] = sc.nextDouble();
			}
			
		while(sc.hasNextInt()) {
			
			int be = sc.nextInt();
			int en = sc.nextInt();
			try {
				System.out.println(ArrayUtils.findMax(ar,be,en));
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		
		try {
		     System.out.println(ArrayUtils.class.getDeclaredMethod("findMax", double[].class,int.class,int.class));
		} catch (Exception e1) {
		}
		
		sc.close();
	}

}

